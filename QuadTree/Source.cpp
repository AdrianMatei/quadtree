#include <iostream>
#include <fstream>
std::ifstream fin("Image_1.txt");
#include <windows.h>
HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
#include <string>

struct NODE
{
	int x, y;
	int size;
	NODE* parent;
	NODE* NW;
	NODE* NE;
	NODE* SW;
	NODE* SE;

	char name = '-';
	int nr = 0;

	NODE(int x, int y, int size, NODE* parent)
	{
		this->x = x;
		this->y = y;
		this->size = size;
		this->parent = parent;
		this->NE = NULL;
		this->NW = NULL;
		this->SE = NULL;
		this->SW = NULL;

		if (this->size == 1)
		{
			static int val = 1;
			this->nr = val++;
		}
		else
		{
			static char letter = 65;
			this->name = letter++;
		}
	}

};

struct MyStack
{
	int elements[100];
	int size = 30;
	int vf = -1;

	bool isEmpty()
	{
		if (vf == -1)
			return 1;
		return 0;
	}

	bool isFull()
	{
		if (vf == size - 1)
			return 1;
		return 0;
	}

	void push(int elem)
	{
		if (isFull())
			std::cout << "stiva e plina \n";
		else
		{
			vf++;
			elements[vf] = elem;
		}
	}

	void pop()
	{
		if (isEmpty())
			std::cout << "stiva e goala";
		else
			vf--;
	}

	void print()
	{
		for (int i = vf; i >= 0; i--)
			std::cout << elements[i] << "\n";
		std::cout << "\n";
	}

	int top()
	{
		return elements[vf];
	}
};

struct MyQueue
{

	NODE** elements;
	int size;
	int last, first;

	MyQueue()
	{
		size = 500;
		first = 0;
		last = 0;
		elements = new NODE*[size];
	}

	bool ISEMPTY()
	{
		if (first == last)
			return 1;
		return 0;
	}

	bool ISFULL()
	{
		if (last == first - 1)
			return 1;
		return 0;
	}

	void PUSH(NODE* elem)
	{
		if (ISFULL())
			std::cout << " Coada e plina \n";
		else
		{
			elements[last] = elem;
			if ((last + 1) % size == first)
				last = 0;
			else
				last++;
		}
	}

	void POP()
	{
		if (ISEMPTY())
		{
			std::cout << " Coada e goala \n";
			first = 0;
		}
		else
			first++;
	}

};

struct Quad_Tree
{
	NODE* root = NULL;
	int n;
	bool** matrix;

	void READ_MATRIX()
	{
		fin >> n;
		matrix = new bool*[n];
		for (int index = 0; index < n; index++)
			matrix[index] = new bool[n];
		for (int row = 0; row < n; row++)
			for (int col = 0; col < n; col++)
				fin >> matrix[row][col];
	}

	void PRINT_MATRIX()
	{
		std::cout << "\n";
		for (int row = 0; row < n; row++)
		{
			for (int col = 0; col < n; col++)
			{
				if (matrix[row][col] == 1)
					SetConsoleTextAttribute(hConsole, 10);
				else
					SetConsoleTextAttribute(hConsole, 12);
				std::cout << " " << matrix[row][col];
			}
			std::cout << "\n";
		}
		std::cout << "\n";
		SetConsoleTextAttribute(hConsole, 7);
	}

	void CREATE_TREE()
	{
		MyQueue Q;
		NODE* X = new NODE(0, 0, n, NULL);
		root = X;
		Q.PUSH(X);
		while (Q.ISEMPTY() == 0)
		{
			X = Q.elements[Q.first];
			if (uniform(matrix, X->x, X->y, X->size) == 1)
				Q.POP();
			else
			{
				X->NW = new NODE(X->x, X->y, X->size / 2, X);
				X->NE = new NODE(X->x, X->y + X->size / 2, X->size / 2, X);
				X->SW = new NODE(X->x + X->size / 2, X->y, X->size / 2, X);
				X->SE = new NODE(X->x + X->size / 2, X->y + X->size / 2, X->size / 2, X);
				Q.PUSH(X->NW);
				Q.PUSH(X->NE);
				Q.PUSH(X->SW);
				Q.PUSH(X->SE);
				Q.POP();
			}
		}
		codes();
	}

	bool uniform(bool** matrix, int x, int y, int size)
	{
		int zero = 0;
		int one = 0;
		for (int row = 0; row < size; row++)
			for (int col = 0; col < size; col++)
			{
				if (matrix[x + row][y + col] == 0)
					zero++;
				else
					if (matrix[x + row][y + col] == 1)
						one++;
			}
		if (zero == 0 || one == 0)
			return 1;
		return 0;
	}

	void codes()
	{
		MyQueue Q;
		int val = 1;
		NODE* X = root;
		Q.PUSH(X);
		while (Q.ISEMPTY() == 0)
		{
			X = Q.elements[Q.first];
			if (X->size == 1)
				X->nr = val++;
			Q.POP();
			while (X->NW != NULL)
			{
				Q.PUSH(X->NW);
				Q.PUSH(X->NE);
				Q.PUSH(X->SW);
				Q.PUSH(X->SE);
				X = X->NW;
			}
		}
	}

	void preorder(NODE* X)
	{
		if (X != NULL)
		{
			if (X->size == 1)
				std::cout << " " << X->nr;
			else
				std::cout << " " << X->name;
			preorder(X->NW);
			preorder(X->NE);
			preorder(X->SW);
			preorder(X->SE);
		}
	}

	void PRINT_SECTIONS()
	{
		MyQueue Q;
		NODE* X = root;
		Q.PUSH(X);
		while (Q.ISEMPTY() == 0)
		{
			X = Q.elements[Q.first];
			Q.POP();
			PRINT_PIECE_2(X);
			if (X->NW != NULL)
			{
				Q.PUSH(X->NW);
				Q.PUSH(X->NE);
				Q.PUSH(X->SW);
				Q.PUSH(X->SE);
			}
		}
	}

	void PRINT_PIECE(NODE* X)
	{
		if (X->size == 1)
			std::cout << " " << X->nr << "\n";
		else
			std::cout << "\n " << X->name << "\n";
		for (int row = 0; row < n; row++)
		{
			for (int col = 0; col < n; col++)
			{
				if (row >= X->x && row < X->x + X->size && col >= X->y && col < X->y + X->size)
				{
					if (matrix[row][col] == 1)
						SetConsoleTextAttribute(hConsole, 11);//26
					else
						if(matrix[row][col] == 0)
							SetConsoleTextAttribute(hConsole, 14);//28
				}
				else
				{
					if (matrix[row][col] == 1)
						SetConsoleTextAttribute(hConsole, 10);
					else
						if (matrix[row][col] == 0)
							SetConsoleTextAttribute(hConsole, 12);
				}
				std::cout << " " << matrix[row][col];
				SetConsoleTextAttribute(hConsole, 7);
			}
			SetConsoleTextAttribute(hConsole, 7);
			std::cout << "\n";
		}
		SetConsoleTextAttribute(hConsole, 7);
		std::cout << "\n";
	}

	void PRINT_PIECE_2(NODE* X)
	{
		if (X->size == 1)
			std::cout << " " << X->nr << "\n";
		else
			std::cout << "\n " << X->name << "\n";
		for (int row = 0; row < n; row++)
		{
			for (int col = 0; col < n; col++)
			{
				if (row >= X->x && row < X->x + X->size && col >= X->y && col < X->y + X->size)
						SetConsoleTextAttribute(hConsole, 11);
				else
					SetConsoleTextAttribute(hConsole, 14);
				std::cout << " " << matrix[row][col];
				SetConsoleTextAttribute(hConsole, 7);
			}
			SetConsoleTextAttribute(hConsole, 7);
			std::cout << "\n";
		}
		SetConsoleTextAttribute(hConsole, 7);
		std::cout << "\n";
	}

	NODE* SEARCH(NODE* X, char name[], int nr)
	{
		MyQueue Q;
		Q.PUSH(X);
		while (Q.ISEMPTY() == 0 && X != NULL)
		{
			X = Q.elements[Q.first];
			Q.POP();
			if ((X->name == '-' && X->nr == nr) || (name != NULL && X->nr == 0 && X->name == name[0]))
				return X;
			while (X->NW != NULL)
			{
				Q.PUSH(X->NW);
				Q.PUSH(X->NE);
				Q.PUSH(X->SW);
				Q.PUSH(X->SE);
				X = X->NW;
			}
		}
		return NULL;
	}

	NODE* GSN(NODE* P, char D)
	{
		// URCARE
		NODE* node = P;
		NODE* parent = node->parent;
		MyStack S;
		while (parent != NULL && checkDirection(node, D) != 0)
		{
			int dir = checkDirection(node, D);
			S.push(dir);
			node = parent;
			parent = node->parent;
		}
		if (parent == NULL)
			return NULL;
		S.push(simetry(node, D));
		node = parent;
		// COBORARE
		while (S.isEmpty() == 0)
		{
			int dir = S.elements[S.vf];
			if (node->NW != NULL)
			{
				if (dir == 1)
					node = node->NW;
				else
					if (dir == 2)
						node = node->NE;
					else
						if (dir == 3)
							node = node->SW;
						else
							if (dir == 4)
								node = node->SE;
				S.pop();
			}
			else
				while (S.isEmpty() == 0)
					S.pop();
		}
		return node;
	}

	int simetry(NODE* X, char D)
	{
		// NW 1
		// NE 2
		// SW 3
		// SE 4
		if (D == 'N')
		{
			if (X == X->parent->SW)
				return 1;
			else
				if (X == X->parent->SE)
					return 2;
		}
		else
		{
			if (D == 'S')
			{
				if (X == X->parent->NW)
					return 3;
				else
					if (X == X->parent->NE)
						return 4;
			}
			else
			{
				if (D == 'E')
				{
					if (X == X->parent->NW)
						return 2;
					else
						if (X == X->parent->SW)
							return 4;
				}
				else
				{
					if (D == 'W')
					{
						if (X == X->parent->NE)
							return 1;
						else
							if (X == X->parent->SE)
								return 3;
					}
				}
			}
		}
		return 0;
	}

	int checkDirection(NODE* X, char D)
	{
		// NW 1
		// NE 2
		// SW 3
		// SE 4
		if (D == 'N')
		{
			if (X == X->parent->NW)
				return 3;
			else
				if (X == X->parent->NE)
					return 4;
		}
		else
		{
			if (D == 'S')
			{
				if (X == X->parent->SW)
					return 1;
				else
					if (X == X->parent->SE)
						return 2;
			}
			else
			{
				if (D == 'E')
				{
					if (X == X->parent->NE)
						return 1;
					else
						if (X == X->parent->SE)
							return 3;
				}
				else
				{
					if (D == 'W')
					{
						if (X == X->parent->NW)
							return 2;
						else
							if (X == X->parent->SW)
								return 4;
					}
				}
			}
		}
		return 0;
	}

	NODE* GCN(NODE* P, char NS, char EW)
	{
		MyStack S;
		NODE* node = P;
		NODE* parent = node->parent;
		int d;
		if (parent == NULL)
			return NULL;
		else
		{
			if (node->name == 'H' && NS == 'S' && EW == 'W')
				std::cout << "";
			d = identicalDirections(node, NS, EW);
			S.push(direction(node));
			if (parent != NULL && d == 3)
			{
				do
				{
					node = parent;
					parent = node->parent;
					if (parent != NULL)
					{
						S.push(direction(node));
						d = identicalDirections(node, NS, EW);
					}
				} while (parent != NULL && d == 3);
			}
			if(parent == NULL)
				return NULL;
			NODE* X;
			if (d == 1)
			{
				X = GSN(parent, NS);
				if (X == NULL)
					return NULL;
				else
				{
					while (X->NW != NULL && S.isEmpty() == 0)
					{
						int dir = S.top();
						S.pop();
						if (dir == 1)
							X = X->NW;
						else
							if (dir == 2)
								X = X->NE;
							else
								if (dir == 3)
									X = X->SW;
								else
									if (dir == 4)
										X = X->SE;
					}
					return X;
				}
			}
			else
			{
				if (d == 2)
				{
					X = GSN(parent, EW);
					if (X == NULL)
						return NULL;
					else
					{
						while (X->NW != NULL && S.isEmpty() == 0)
						{
							int dir = S.top();
							S.pop();
							if (dir == 1)
								X = X->NW;
							else
								if (dir == 2)
									X = X->NE;
								else
									if (dir == 3)
										X = X->SW;
									else
										if (dir == 4)
											X = X->SE;
						}
						return X;
					}
				}
				else
				{
					if (d == 0)
					{
						X = parent;
						while (X->NW != NULL && S.isEmpty() == 0)
						{
							int dir = S.top();
							S.pop();
							if (dir == 1)
								X = X->NW;
							else
								if (dir == 2)
									X = X->NE;
								else
									if (dir == 3)
										X = X->SW;
									else
										if (dir == 4)
											X = X->SE;
						}
						return X;
					}
				}
			}
		}
		
	}

	/*NODE* GCN(NODE* P, char NS, char EW)
	{
		MyStack S;
		NODE* node = P;
		NODE* parent = node->parent;
		int d = -1;
		if (node->parent != NULL)
		{
			d = identicalDirections(node, NS, EW);
			while(node != NULL && d == 3)
			{
				S.push(direction(node));
				node = parent;
				parent = node->parent;
				if(parent != NULL)
					d = identicalDirections(node, NS, EW);
			}
			if (node == NULL)
				return NULL;
			else
			{
				if (d == 1)
				{
					node = GSN(node, NS);
					while (node != NULL && node->NW != NULL && S.isEmpty() == 0)
					{
						int dir = S.top();
						S.pop();
						if (dir == 1)
							node = node->NW;
						else
							if (dir == 2)
								node = node->NE;
							else
								if (dir == 3)
									node = node->SW;
								else
									if (dir == 4)
										node = node->SE;
					}
				}
				else
				{
					if (d == 2)
					{
						node = GSN(node, EW);
						while (node != NULL && node->NW != NULL && S.isEmpty() == 0)
						{
							int dir = S.top();
							S.pop();
							if (dir == 1)
								node = node->NW;
							else
								if (dir == 2)
									node = node->NE;
								else
									if (dir == 3)
										node = node->SW;
									else
										if (dir == 4)
											node = node->SE;
						}
					}
					else
					{
						if (d == 0)
						{
							while (node != NULL && node->NW != NULL && S.isEmpty() == 0)
							{
								int dir = S.top();
								S.pop();
								if (dir == 1)
									node = node->NW;
								else
									if (dir == 2)
										node = node->NE;
									else
										if (dir == 3)
											node = node->SW;
										else
											if (dir == 4)
												node = node->SE;
							}
						}
					}
				}
			}
			return node;
		}
	}*/

	int identicalDirections(NODE* X, char NS, char EW)
	{
		char D1, D2;
		if (X == X->parent->NW)
		{
			D1 = 'N';
			D2 = 'W';
		}
		else
		{
			if (X == X->parent->NE)
			{
				D1 = 'N';
				D2 = 'E';
			}
			else
			{
				if (X == X->parent->SW)
				{
					D1 = 'S';
					D2 = 'W';
				}
				else
				{
					if (X == X->parent->SE)
					{
						D1 = 'S';
						D2 = 'E';
					}
				}
			}
		}
		if (D1 == NS && D2 == EW)
			return 3;
		else
		{
			if (D1 == NS)
				return 1;
			else
			{
				if (D2 == EW)
					return 2;
				else
					return 0;
			}
		}
	}

	int direction(NODE* X)
	{
		// NW 1
		// NE 2
		// SW 3
		// SE 4
		if (X == X->parent->NW)
			return 4;
		else
			if (X == X->parent->NE)
				return 3;
			else
				if (X == X->parent->SW)
					return 2;
				else
					if (X == X->parent->SE)
						return 1;
	}

	void allGSN()
	{
		MyQueue Q;
		NODE* X = root;
		Q.PUSH(X);
		std::cout << "\n";
		while (Q.ISEMPTY() == 0)
		{
			X = Q.elements[Q.first];
			if (X != NULL)
			{
				char i;
				for (int d = 0; d < 4; d++)
				{
					char direction;
					if (d == 0)
						direction = 'N';
					else
						if (d == 1)
							direction = 'S';
						else
							if (d == 2)
								direction = 'E';
							else
								if (d == 3)
									direction = 'W';
					if (X->name == '-')
					{
						i = X->nr;
						std::cout << " GSN(" << X->nr << ", " << direction << ") = ";
					}
					else
					{
						i = X->name;
						std::cout << " GSN(" << X->name << ", " << direction << ") = ";
					}
					if (X != NULL)
					{
						NODE* REZ = GSN(X, direction);
						if (REZ != NULL)
						{
							if (REZ->size == 1)
								std::cout << REZ->nr;
							else
								std::cout << REZ->name;
						}
						else
							std::cout << "NU EXISTA!";
						std::cout << "\n";
					}
				}
				std::cout << "\n";
			}
			if (X != NULL)
			{
				Q.PUSH(X->NW);
				Q.PUSH(X->NE);
				Q.PUSH(X->SW);
				Q.PUSH(X->SE);
			}
			Q.POP();
		}
		std::cout << "\n\n\n";
	}

	void allGCN()
	{
		MyQueue Q;
		NODE* X = root;
		Q.PUSH(X);
		std::cout << "\n";
		while (Q.ISEMPTY() == 0)
		{
			X = Q.elements[Q.first];
			if (X != NULL)
			{
				char i;
				for (int d = 0; d < 4; d++)
				{
					char direction1;
					char direction2;
					if (d == 0)
					{
						direction1 = 'N';
						direction2 = 'W';
					}
					else
						if (d == 1)
						{
							direction1 = 'N';
							direction2 = 'E';
						}
						else
							if (d == 2)
							{
								direction1 = 'S';
								direction2 = 'W';
							}
							else
								if (d == 3)
								{
									direction1 = 'S';
									direction2 = 'E';
								}
					if (X->name == '-')
					{
						i = X->nr;
						std::cout << " GCN(" << X->nr << ", " << direction1 << direction2 << ") = ";
					}
					else
					{
						i = X->name;
						std::cout << " GCN(" << X->name << ", " << direction1 << direction2 << ") = ";
					}
					if (X != NULL)
					{
						NODE* REZ = GCN(X, direction1, direction2);
						if (REZ != NULL)
						{
							if (REZ->size == 1)
								std::cout << REZ->nr;
							else
								std::cout << REZ->name;
						}
						else
							std::cout << "NU EXISTA!";
						std::cout << "\n";
					}
				}
				std::cout << "\n";
			}
			if (X != NULL)
			{
				Q.PUSH(X->NW);
				Q.PUSH(X->NE);
				Q.PUSH(X->SW);
				Q.PUSH(X->SE);
			}
			Q.POP();
		}
		std::cout << "\n\n\n";
	}

};

bool isNumber(char* s)
{
	for (int i = 0; i < strlen(s); i++)
		if (isdigit(s[i]) == false)
			return false;
	return true;
}

void menu(Quad_Tree QT)
{
	std::cout << "\n   1. Afisare arbore (preordine)";
	std::cout << "\n   2. Afisare regiuni";
	std::cout << "\n   3. GSN(P, D)";
	std::cout << "\n   4. GCN(P, D)";
	std::cout << "\n   5. Toate GSN-urile";
	std::cout << "\n   6. Toate GCN-urile";
	std::cout << "\n   7. Inchidere program";
	std::cout << "\n\n Optiune: ";
}

int main()
{
	Quad_Tree QT;
	QT.READ_MATRIX();
	QT.PRINT_MATRIX();
	system("pause");
	QT.CREATE_TREE();
	int option = 0;
	NODE* X;
	char i[2];
	while (option != 7)
	{
		system("CLS");
		std::cout << "\n";
		QT.preorder(QT.root);
		std::cout << "\n\n\n";
		menu(QT);
		std::cin >> option;
		system("CLS");
		switch (option)
		{
			case 1: QT.preorder(QT.root);
					std::cout << "\n\n\n";
					break;

			case 2:	QT.PRINT_SECTIONS();
					break;

			case 3: char D;
					std::cout << "\n GSN(P, D)";
					std::cout << "\n   P = ";
					std::cin >> i;
					std::cout << "   D = ";
					std::cin >> D;
					if (isNumber(i))
					{
						int number = std::stoi(i);
						X = QT.SEARCH(QT.root, NULL, number);
					}
					else
						X = QT.SEARCH(QT.root, i, 0);
					if (X != NULL)
					{
						std::cout << " REZ = ";
						NODE* REZ = QT.GSN(X, D);
						if (REZ != NULL)
						{
							if (REZ->size == 1)
								std::cout << REZ->nr;
							else
								std::cout << REZ->name;
						}
						else
							std::cout << " NU EXISTA!";
						std::cout << "\n\n";
					}
					break;

			case 4: char NS, EW;
					std::cout << "\n GCN(P, D)";
					std::cout << "\n   P = ";
					std::cin >> i;
					std::cout << "   D = ";
					std::cin >> NS >> EW;
					if (isNumber(i))
					{
						int number = std::stoi(i);
						X = QT.SEARCH(QT.root, NULL, number);
					}
					else
						X = QT.SEARCH(QT.root, i, 0);
					if (X != NULL)
					{
						std::cout << " REZ = ";
						NODE* REZ = QT.GCN(X, NS, EW);
						if (REZ != NULL)
						{
							if (REZ->size == 1)
								std::cout << REZ->nr;
							else
								std::cout << REZ->name;
						}
						else
							std::cout << " NU EXISTA!";
						std::cout << "\n\n";
					}
					break;

			case 5: QT.allGSN();
					break;

			case 6: QT.allGCN();
					break;
		}
		if (option != 7)
			system("pause");
	}
	return 0;
}